import Vue from "vue";
import Router from "vue-router";
import Login from "@/components/GeneralViews/Login";
import Index from "@/components/GeneralViews/Index";
import Projetos from "@/components/GeneralViews/Projetos";
import Cadastrar from "@/components/GeneralViews/Cadastrar";
import Conta from "@/components/Conta";
import Template from "@/components/GeneralViews/Template";
import Sobre from "@/components/GeneralViews/Sobre";
import Teste from "@/components/GeneralViews/Teste";
import CriarCard from "@/components/GeneralViews/CriarCard"
Vue.use(Router);

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Index",
      component: Index
    },
    {
      path: "/login",
      name: "Login",
      component: Login
    },
    {
      path: "/projetos",
      name: "Projetos",
      component: Projetos
    },
    {
      path: "/cadastrar",
      name: "Cadastrar",
      component: Cadastrar
    },
    {
      path: "/conta",
      name: "Conta",
      component: Conta
    },
    {
      path: "/jogo/:id",
      name: "Template",
      component: Template
    },
    {
      path: "/sobre",
      name: "Sobre",
      component: Sobre
    },
    {
    path: "/teste",
    name: "Teste",
    component: Teste
    },
    {
    path: "/card",
    name: "CriarCard",
    component: CriarCard  
    }
  ]
});
